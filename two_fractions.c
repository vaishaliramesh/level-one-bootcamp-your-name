
#include<stdio.h>
struct fract
{
    int num;
    int deno;
};
typedef struct fract fraction;
fraction input()
{
    fraction a;
    printf("Enter the numerator\n");
    scanf("%d",&a.num);
    printf("Enter the denominator\n");
    scanf("%d",&a.deno);
    return a;
}
int gcd(int a,int b)
{
    int g=1;
    for(int i=2;i<=a&&i<=b;i++)
    {
         if(a%i==0&&b%i==0)
         {
             g=i;
          }
     }
    return g;
}

fraction sum(fraction a, fraction b)
{
    fraction res;
    res.deno=a.deno*b.deno;
    res.num=(a.num*b.deno)+(b.num*a.deno);
    int g=gcd(res.num,res.deno);
    res.num=res.num/g;
    res.deno=res.deno/g;
    return res;
}

void output(fraction a,fraction b,fraction c)
{
 printf("the sum of %d / %d and %d / %d is %d / %d", a.num,a.deno,b.num,b.deno,c.num,c.deno);
}

int main()
{
fraction a,b,c;
a=input();
b=input();
c=sum(a,b);
output(a,b,c);
return 0;
}
